﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class BossGenerator : Generator
{
    #region Fields

    [SerializeField] protected Boss   m_zombie                = null;                 //prefab que vai ser criado
    protected float                               m_timeToSpawnZombie     = 10f;                 //tempo de sapwn dos zumbis



    #endregion

    #region Methods

    private void Awake ( )
    {
        //GameManager._Instance.MQtdBossTotal = 1;
        //m_nextTimeDificulty = 20;
    }

    protected override void UpdateGenerator()
    {
        if (GameManager._Instance.m_playerController.m_hp.IsAlive ( ) && Vector3.Distance ( transform.position, GameManager._Instance.m_playerTransform.position ) > C_DISTANCE_PLAYER )
        {
            //contador de tempo
            m_contTime += Time.deltaTime;

            //tempo de spawn aleatorio
            m_timeToSpawnZombie = Random.Range ( 7, 20 );

            if ( m_contTime >= m_timeToSpawnZombie )
            {
                //check se pode spawnar
                if ( CheckSpawn ( ) )
                {
                    //spawna zumbi
                    Boss zombie = Instantiate ( m_zombie, AleatoryPosition ( ), transform.rotation );
                    zombie.SetupEnemy ( zombie, this );

                    AddZombie ( zombie );
                }
                //zera contador
                m_contTime = 0f;
            }

            ////aumentar dificuldade
            //if ( GameManager._Instance.GetTimeFloat ( ) >= m_nextTimeDificulty )
            //{
            //    GameManager._Instance.MQtdBossTotal++;
            //    m_nextTimeDificulty += 60f;
            //}
        }
    }

    #endregion

    protected bool CheckSpawn ( )
    {
        //ve quantos inimigos tem em um raio
        Collider [ ] cols = Physics.OverlapSphere ( transform.position, 1, m_layerZombie );


        //ver se tem algum zumbi no lugar de spawn ou se a quantidade de zumbis for a maxima
        if ( cols.Length > 0 || GameManager._Instance.MQtdBossAlive >= GameManager._Instance.MQtdBossTotal ) return false;
        else return true;
    }

    #region Gizmos

    private void OnDrawGizmos ( )
    {
         Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere ( transform.position, C_RAY_GENERATOR);
    }

    #endregion
}