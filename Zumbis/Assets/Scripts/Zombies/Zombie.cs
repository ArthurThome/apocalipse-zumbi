﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

public class Zombie : ZombieController
{
    #region Fields

    private Coroutine m_moveCoroutine = null;
    public float                         m_distanceToHit         = 3.5f;             //distancia do collider do player e do zumbi

    #endregion

    #region Setup Enemy

    public override void SetupEnemy ( ZombieController _zombie, Generator _generator )
    {
        base.SetupEnemy(_zombie, _generator);

        m_distanceToHit = 2.5f;

        //escolher avatar do zumbi
        transform.GetChild ( Random.Range ( 1, 28 ) ).gameObject.SetActive ( true );

        //definir vida do zumbi
        m_hp.SetupHP ( 20 );
    }

    #endregion

    #region MonoBehavior Methods

    private void Awake()
    {
        m_hp = GetComponent<GenericHP>();
    }

    private void FixedUpdate()
    {
        //ver se player esta vivo
        if (GameManager._Instance.m_playerController.m_hp.IsAlive())
        {
            //animacao do zumbi
            //m_animatorZombie.SetFloat("Walk", m_dir.magnitude);
            m_animatorZombie.SetFloat ( "Walk", m_navMeshZombie.velocity.magnitude );

            //ver a distancia do zumbi e do player
            //se for menor que c_mindistance e maior que distance to hit, anda ate o player
            if (!DistanceCalculate(GameManager._Instance.m_playerTransform.position, C_MIN_DISTANCE) && DistanceCalculate(GameManager._Instance.m_playerTransform.position, m_distanceToHit))
            {
                //animacao zumbi
                m_animatorZombie.SetBool ( "Ataque", false );

                //direcao do player, que o zumbi vai andar
                m_dir = GameManager._Instance.m_playerTransform.position - transform.position;

                //zumbi segue o player
                //m_rigidbodyZombie.MovePosition(m_rigidbodyZombie.position + (m_dir.normalized * m_speed * Time.deltaTime));
                m_navMeshZombie.SetDestination ( GameManager._Instance.m_playerTransform.position );

                Rotation ( );
            }
            //se for menos que 3 ataca o player
            else if (!DistanceCalculate(GameManager._Instance.m_playerTransform.position, m_distanceToHit))
            {
                //direcao do player, que o zumbi vai atacar
                m_dir = GameManager._Instance.m_playerTransform.position - transform.position;

                m_animatorZombie.SetBool("Ataque", true);

                Rotation ( );
            }
            else
            {
                //andar aleatorio
                //Roaming ( );
                if ( m_moveCoroutine == null ) m_moveCoroutine = StartCoroutine ( WaitToRoaming ( ) );

                //se o player nao estiver perto, starta corrotina
            }
        }
        else 
        {
            m_navMeshZombie.SetDestination ( transform.position );
        }
    }

    #endregion

    protected void Roaming ( )
    {
        if ( !DistanceCalculate ( m_navMeshZombie.destination, 2.6f ) )
        {
            m_aleatoryPosition = AleatoryPosition ( );
            m_dir = m_aleatoryPosition - transform.position;
                
            //cheacr se destino esta no navmesh

            //zumbi fica vagando
            m_navMeshZombie.SetDestination ( m_rigidbodyZombie.position + m_dir.normalized * m_speed );

        }

        StopAllCoroutines ( );
        m_moveCoroutine = null;
    }

    private IEnumerator WaitToRoaming ( )
    {
         yield return new WaitForSeconds ( 5 );

        Roaming ( );
    }
}
