﻿// Arthur Martins Thomé
// 24 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class ZombieGenerator : Generator
{
    #region Fields

    [SerializeField] protected Zombie   m_zombie                = null;                 //prefab que vai ser criado
    protected float                               m_timeToSpawnZombie     = 2f;                 //tempo de sapwn dos zumbis

    #endregion

    #region Methods

    protected override void UpdateGenerator ( )
    {
        if ( GameManager._Instance.m_playerController.m_hp.IsAlive ( ) && Vector3.Distance ( transform.position, GameManager._Instance.m_playerTransform.position ) > C_DISTANCE_PLAYER)
        {
            //contador de tempo
            m_contTime += Time.deltaTime;

            //check se pode spawnar
            if ( CheckSpawn ( ) && m_contTime >= m_timeToSpawnZombie )
            {
                //spawna zumbi
                Zombie zombie = Instantiate ( m_zombie, AleatoryPosition ( ), transform.rotation );
                zombie.SetupEnemy ( zombie, this );

                AddZombie ( zombie );
                //zera contador
                m_contTime = 0f;
            }
        }

        //aumentar dificuldade
        if ( GameManager._Instance.GetTimeFloat ( ) >= m_nextTimeDificulty )
        {
            m_qtdZombieAliveMax++;
            m_nextTimeDificulty += 20f;
        }
    }

    #endregion

    protected bool CheckSpawn ( )
    {
        //ve quantos inimigos tem em um raio
        Collider [ ] cols = Physics.OverlapSphere ( transform.position, 1, m_layerZombie );


        //ver se tem algum zumbi no lugar de spawn ou se a quantidade de zumbis for a maxima
        if ( cols.Length > 0 || m_qtdZombieAlive >= m_qtdZombieAliveMax ) return false;
        else return true;
    }

    #region Gizmos

    private void OnDrawGizmos ( )
    {
         Gizmos.color = Color.red;
        Gizmos.DrawWireSphere ( transform.position, C_RAY_GENERATOR );
    }

    #endregion

}