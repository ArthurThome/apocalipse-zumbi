﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

public class Boss : ZombieController
{
    #region Fields

    protected float                         m_distanceToHit         = 4.5f;             //distancia do collider do player e do zumbi

    #endregion

    #region Setup Enemy

    public override void SetupEnemy ( ZombieController _zombie, Generator _generator )
    {
        base.SetupEnemy ( _zombie, _generator );

        m_distanceToHit = 4.5f;
        m_strength = 30;
        m_pointsWhenDie = 5;


        //definir vida do zumbi
        m_hp.SetupHP ( 250 );
    }

    #endregion

    #region MonoBehavior Methods

    protected override void UpdateZombie ( )
    {
        //ver se player esta vivo
        if ( GameManager._Instance.m_playerController.m_hp.IsAlive ( ) )
        {
            //animacao do boss
            m_animatorZombie.SetFloat ( "Walk", m_navMeshZombie.velocity.magnitude );

            //sedguir o player
            m_navMeshZombie.SetDestination ( GameManager._Instance.m_playerTransform.position );
            

            if ( m_navMeshZombie.remainingDistance <= m_distanceToHit )
            {
                //direcao do player, que o zumbi vai atacar
                m_dir = GameManager._Instance.m_playerTransform.position - transform.position;

                m_animatorZombie.SetBool ( "Ataque", true );
            }
            else
            {
                m_animatorZombie.SetBool ( "Ataque", false );
            }
            Rotation ( );
        }
        else 
        {
            m_navMeshZombie.SetDestination ( transform.position );
        }
    }

    #endregion
}
