﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class Generator : MonoBehaviour
{
    #region Fields

    protected const float                         C_DISTANCE_PLAYER       = 15f;              //distancia do player em relação ao spawn
    protected const float                         C_RAY_GENERATOR         = 3;                //raido do gerador

    protected uint m_qtdZombieAliveMax = 2;
    protected uint m_qtdZombieAlive = 0;

    protected float                               m_nextTimeDificulty     = 0f;
    
    protected float                               m_contTime              = 0f;                   //time to spawn
    [SerializeField] protected LayerMask          m_layerZombie           = new LayerMask ( );    //layer do zumbi
    

    #endregion


    #region Methods

    private void Update ( )
    {
        UpdateGenerator ( );
    }

    protected virtual void UpdateGenerator ( ) { }

    #endregion

    protected Vector3 AleatoryPosition ( )
    {
        Vector3 position = Random.insideUnitSphere * C_RAY_GENERATOR;
        position += transform.position;
        position.y = transform.position.y;

        return position;
    }

    public void AddZombie ( ZombieController _zombie )
    {
        if ( _zombie.GetComponent<Zombie> ( ) != null )
            m_qtdZombieAlive++;
        else if ( _zombie.GetComponent<Boss> ( ) != null )
            GameManager._Instance.MQtdBossAlive++;
    }

    public void DeleteZombie ( ZombieController _zombie )
    {
        if ( _zombie.GetComponent<Zombie> ( ) != null )
            m_qtdZombieAlive--;
        else if ( _zombie.GetComponent<Boss> ( ) != null )
            GameManager._Instance.MQtdBossAlive--;
    }

    
}
