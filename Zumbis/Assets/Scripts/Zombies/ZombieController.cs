﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

#endregion

public class ZombieController : MonoBehaviour
{
    #region Fields

    protected const float                   C_MIN_DISTANCE          = 30F;              //distancia pro zumbi perseguir o player
    protected const byte                    C_TIME_TO_ROMING        = 6;                //tempo que demora pro zumbi vagar

    protected float                         m_countRoaming          = 0f;
    protected int                           m_pointsWhenDie         = 1;
    

    public GenericHP                        m_hp                    = null;             //script GenericHP
    protected byte                          m_strength              = 10;               //dano do zumbi
    protected Vector3                       m_dir                   = new Vector3 ( );
    protected Vector3                       m_aleatoryPosition      = new Vector3 ( );

    [SerializeField] protected float        m_speed                 = 5f;               //speed zombie
    [SerializeField] public Animator        m_animatorZombie        = null;             //animator zumbie
    protected ZombieController              m_zombieReference       = null;             //referencia zombie
    protected Rigidbody                     m_rigidbodyZombie       = null;
    protected Generator                     m_zombieGenerator       = null;
    [SerializeField] protected NavMeshAgent m_navMeshZombie         = null;

    [SerializeField] public AudioClip   m_killedSound               = null;     //som de morte do zumbi

    #endregion

    #region Setup Enemy

    public virtual void SetupEnemy ( ZombieController _zombie, Generator _generator )
    {
        m_zombieReference = _zombie;

        m_rigidbodyZombie = _zombie.GetComponent<Rigidbody> ( );

        //referencia do gerador
        m_zombieGenerator = _generator;
    }

    #endregion

    #region MonoBehavior Methods

    private void Awake ( )
    {
        m_hp = GetComponent < GenericHP > ( );
    }

    private void FixedUpdate ( )
    {
        UpdateZombie ( );
    }

    #endregion

    protected virtual void UpdateZombie ( ) { }

    #region Events Animation

    public void HitPlayer ( )
    {
        //metodo de atacar jogador
        GameManager._Instance.m_playerController.damagePlayer ( m_strength );

        if ( m_animatorZombie != null ) m_animatorZombie.SetBool ( "Ataque", false );
    }

    #endregion


    protected Vector3 AleatoryPosition ( )
    {
        Vector3 position = Random.insideUnitSphere * C_TIME_TO_ROMING;
        position += m_rigidbodyZombie.position;
        position.y = 0.1f;

        return position;
    }

    protected void Rotation ( )
    {
        if ( DistanceCalculate ( m_aleatoryPosition, 0.05f ) )
        {
            if ( m_dir == Vector3.zero ) m_rigidbodyZombie.MoveRotation ( Quaternion.LookRotation ( m_dir ) );
        }
    }

    protected bool DistanceCalculate ( Vector3 _position, float _distance )
    {
        if ( Vector3.Distance ( transform.position, _position ) >= _distance ) return true;
        else return false;
    }

    public void KillZombie ( )
    {
        m_zombieReference.m_animatorZombie.SetTrigger ( "Die" );

        //som de dano no zumbi
        GameManager._Instance.m_audioController.PlayOneShot ( m_killedSound );
                
        //ver se dropa ou nao um item
        GameManager._Instance.m_itemController.DropItem ( gameObject.transform.position );
                
        //destroy zombie
        StartCoroutine ( WaitToDestroy ( ) );

        //adiciona um ponto
        GameManager._Instance.m_persistentController.AddPoint ( m_pointsWhenDie );

        //tira um do contador de zumbis
        m_zombieReference.m_zombieGenerator.DeleteZombie ( this );

        //collider como trigger
        m_zombieReference.GetComponent<Collider> ( ).enabled = false;

        //zerar velocidade rigidbody
        m_zombieReference.m_navMeshZombie.speed = 0;

        enabled = false;
    }

    private IEnumerator WaitToDestroy ( )
    {
        yield return new WaitForSeconds ( 5 );

        Destroy ( m_zombieReference.gameObject );
    }
}