﻿// Arthur Martins Thomé
// 24 APR 2019


#region Using Statements

using UnityEngine;

#endregion

public delegate void OnUpdateHPUI ( int hp );

public class GenericHP : MonoBehaviour
{

    #region Events

    public OnUpdateHPUI UpdateHPUI = null;

    #endregion


    #region Fields

    [SerializeField] private int m_hp = 100;
    [SerializeField] private int m_maxHp = 100;

    #endregion


    #region Monobehaviour Methods


    #endregion


    #region HP Methods

    public void SetupHP ( int max_hp )
    {
        m_maxHp = max_hp;
        m_hp = m_maxHp;
    }

    public void SetupHP ( int min_hp, int max_hp )
    {
        m_maxHp = max_hp;
        m_hp = min_hp;
    }

    //dar dano nas coisas
    public void ApplyDamage ( byte damage )
    {
        //aplicar dano
        m_hp -= damage;

        //update life bar
        GameManager._Instance.m_persistentController.UpdatePersistentCanvas ( );
    }

    //dar vida para as coisas
    public void GiveLife ( int life )
    {
        m_hp += life;
        if ( m_hp > m_maxHp ) m_hp = m_maxHp;

        //update life bar
        GameManager._Instance.m_persistentController.UpdatePersistentCanvas ( );
    }

    public int GetMaxHP ( )
    {
        return m_maxHp;
    }

    public int GetHp ( )
    {
        return m_hp;
    }

    public bool IsAlive ( )
    {
        return m_hp > 0;
    }

    public bool IsFullHp ( )
    {
        return m_hp >= m_maxHp;
    }

    public float GetHPPercentage ( )
    {
        return ( float ) ( ( float ) m_hp / ( float ) m_maxHp );
    }

    #endregion

}