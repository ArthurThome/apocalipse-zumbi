﻿// Arthur Martins Thomé
// 22 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class PlayerController : MonoBehaviour
{
    #region Fields

    private Vector3                         dir                 = new Vector3 ( );
    [SerializeField] private float          m_speed             = 10;               //velocidade de movimentacao do player
    [SerializeField] private LayerMask      m_floorMask;                            //layer do chao

    [SerializeField] public GenericHP       m_hp                = null;             //script GenericHP
    [SerializeField] public Animator        m_animatorPlayer    = null;             //animator player
    [SerializeField] public Rigidbody       m_rigidbodyPlayer   = null;             //rigidbody player

    [SerializeField] public AudioClip       m_damageSound       = null;             //som de dano


    #endregion

    #region MonoBeahvior Methods

    public void SetupPlayer ( )
    {
        //quando for instanciar o player
    }

    private void FixedUpdate ( )
    {
        if ( GameManager._Instance.m_playerController.m_hp.IsAlive ( ) )
        { 
            //direcao que vai se mover
            dir = new Vector3 ( Input.GetAxis ( "Horizontal" ), 0, Input.GetAxis ( "Vertical" ) );

            //set variavel da animacao
            if ( dir != Vector3.zero ) 
                m_animatorPlayer.SetFloat ( "Run", dir.magnitude );
            else 
                m_animatorPlayer.SetFloat ( "Run", dir.magnitude );

            //move o player em uma direcao...
            m_rigidbodyPlayer.MovePosition ( m_rigidbodyPlayer.position + ( dir * Time.deltaTime * m_speed  ) );

            Ray ray = Camera.main.ScreenPointToRay ( Input.mousePosition );
            RaycastHit hit;

            if ( Physics.Raycast ( ray, out hit, 100, m_floorMask )   )
            {
                Vector3 aimPlayer = hit.point - transform.position;
                aimPlayer.y = 0;

                Quaternion newRotation = Quaternion.LookRotation ( aimPlayer );

                m_rigidbodyPlayer.MoveRotation ( newRotation );
            }
        }
    }

    #endregion

    #region Animator Player

    public void SetAnimationDie ( bool _status )
    {
        m_animatorPlayer.SetBool ( "Die", _status );

        m_animatorPlayer.SetLayerWeight ( 1, 0 );
    }

    #endregion

    #region Damage Player

    public void damagePlayer ( byte _strength )
    {
        //aplicar dano no player
        GameManager._Instance.m_playerController.m_hp.ApplyDamage ( _strength );
        
        //som de dano
        GameManager._Instance.m_audioController.PlayOneShot ( m_damageSound );

        //se o jogador morrer, mostra game over
        if ( !GameManager._Instance.m_playerController.m_hp.IsAlive ( ) ) GameManager._Instance.m_panelGameOver.SetStatusTxtGameOver ( );
    }

    #endregion

}
