﻿// Arthur Martins Thomé
// 26 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

#endregion

public class ItemController : MonoBehaviour
{
    #region Fields

    private const float C_TIME_TO_DESTROY = 7f;
    
    [SerializeField] private LayerMask m_itemMask;

     //[ Header ( "List Item" ) ]
    //[SerializeField] public List<Item>              m_listItemController    = new List<Item> ( ); 

    [ Header ( "First Aid" ) ]
    [SerializeField] private ItemController m_prefabFirstAid = null;
    [SerializeField] public byte m_lifeToRecovery = 10; //vida que vai regenerar
    private float m_percentageGenerator = 0.05f;

    #endregion

    private void Start ( )
    {
        //Destroy ( gameObeject, 10 );
        StartCoroutine ( TimeToDestroy ( ) );
    }

    #region Setup Item

    //temporario, ja que so tem first aid
    public void SetupItem ( byte _life )
    {
        m_lifeToRecovery = _life;
    }

    #endregion


    #region Drop Item

    public void DropItem ( Vector3 _position )
    {
        if ( Random.value >= m_percentageGenerator ) return;
        //dropar item
        ItemController item = Instantiate ( m_prefabFirstAid, _position, Quaternion.identity );

        SetupItem ( 10 );
    }

    #endregion

    #region Trigger

    private void OnTriggerEnter ( Collider other )
    {
        if ( other.tag == "Player" )
            FirstAid ( );
        //GameManager._Instance.ShowPanel ( GameManager._Instance.m_persistentController.m_panelInteract.gameObject );
    }

    #endregion

    #region Item Methods

    private void FirstAid ( )
    {
        //ver se esta de vida cheia
        if ( GameManager._Instance.m_playerController.m_hp.IsFullHp ( ) ) return;

        //recupera vida do player
        GameManager._Instance.m_playerController.m_hp.GiveLife ( m_lifeToRecovery );

        Destroy ( gameObject );

        StopCoroutine ( TimeToDestroy ( ) );
    }

    #endregion

    private IEnumerator TimeToDestroy ( )
    {
        yield return new WaitForSeconds ( C_TIME_TO_DESTROY );
        Destroy ( gameObject );
    }
}
