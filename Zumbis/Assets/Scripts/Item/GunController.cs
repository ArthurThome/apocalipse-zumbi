﻿// Arthur Martins Thomé
// 23 APR 2019


#region  Using Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

#endregion

public class GunController : MonoBehaviour
{
    #region Fields

    [SerializeField] private MunitionController         m_munition              = null;     //objeto munition
    [SerializeField] private Transform                  m_positionMunition      = null;     //posicao da municao

    [SerializeField] public AudioClip                   m_shotSound             = null;     //som de dano

    #endregion

    #region MonoBehavior Methods

    private void Update ( )
    {
        //ver se player esta vivo
        if ( Input.GetMouseButtonDown ( 0 ) && GameManager._Instance.m_playerController.m_hp.IsAlive ( ) && !EventSystem.current.IsPointerOverGameObject ( ) )
        {
            //cria o tiro no game
            MunitionController WidgetMunition = Instantiate ( m_munition, m_positionMunition.position, transform.rotation );
            WidgetMunition.SetupMunition ( );

            //som do tiro
            GameManager._Instance.m_audioController.PlayOneShot ( m_shotSound );
        }
    }

    #endregion
}
