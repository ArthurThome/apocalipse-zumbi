﻿// Arthur Martins Thomé
// 23 APR 2019


#region  Using Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class MunitionController : MonoBehaviour
{
    #region Fields

    private byte                        m_munitionDamage    = 10; //dano do tiro
    private Vector3                     m_posInitial        = new Vector3 ( );
    private Zombie                      m_zombieReference  = null; //referencia do zumbi que levou o tiro
    private Boss                        m_bossReference  = null; //referencia do zumbi que levou o tiro
    [SerializeField] private float      speed               = 20f; //speed munition
    [SerializeField] private Rigidbody  m_rigidbodyMunition = null; //objeto munition
    

    
    #endregion

    #region MonoBehavior Methods

    public void SetupMunition ( )
    {
        //save initial position of munition
        m_posInitial = transform.position;

        StartCoroutine ( TrailMunition ( ) );
    }

    private void OnTriggerEnter ( Collider other )
    {
        //destroy munition
        Destroy ( gameObject );

        //destroy zombie
        if ( other.tag == "Inimigo" )
        {
            //grava o inimigo
            m_zombieReference = other.GetComponent <Zombie> ( );

            if ( m_zombieReference != null )
            {
                HitZombie ( m_zombieReference );
            }
            else
            {
                m_bossReference = other.GetComponent <Boss> ( );

                //aplica dano
                HitZombie ( m_bossReference );
            }

        }
    }

    #endregion

    private void HitZombie ( ZombieController _zumbi )
    {
        //aplica dano
        _zumbi.m_hp.ApplyDamage ( m_munitionDamage );
            
        if ( !_zumbi.m_hp.IsAlive ( ) )
            _zumbi.KillZombie ( );
    }

    #region IEnumerator

    private IEnumerator TrailMunition ( )
    {
        float timeToDestroy = 5f;

        do
        {
            yield return null;

            m_rigidbodyMunition.MovePosition ( new Vector3 ( m_rigidbodyMunition.position.x + transform.forward.x * speed * Time.deltaTime, transform.position.y, m_rigidbodyMunition.position.z + transform.forward.z * speed * Time.deltaTime  ) );

            timeToDestroy -= Time.deltaTime;
        } while ( timeToDestroy > 0 );

        Destroy ( gameObject );
    }

    #endregion
}