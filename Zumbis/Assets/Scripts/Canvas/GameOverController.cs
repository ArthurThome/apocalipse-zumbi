﻿// Arthur Martins Thomé
// 25 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

#endregion

public class GameOverController : MonoBehaviour
{
    #region Fields

    [SerializeField] private TextMeshProUGUI        m_playerPointsTxt           = null; //pontos do jogador
    [SerializeField] private TextMeshProUGUI        m_playerPointsTxtRecord     = null; //pontos do jogador recorde
    [SerializeField] private TextMeshProUGUI        m_liveTime                  = null; //tempo vivo
    [SerializeField] private TextMeshProUGUI        m_liveTimeRecordTxt         = null; //tempo vivo recorde

    #endregion

    #region Update Life Bar

    public void UpdateGameOverPanel ( )
    {
        //pontos do player
        if ( m_playerPointsTxt != null ) m_playerPointsTxt.text = GameManager._Instance.m_persistentController.GetPoints ( ).ToString ( );
        //tempo vivo
        if ( m_liveTime != null ) m_liveTime.text = GameManager._Instance.GetTime ( );
        //record time
        if ( m_liveTimeRecordTxt != null ) m_liveTimeRecordTxt.text = GameManager._Instance.GetTimeRecord ( );
        //record pontos
        if ( m_playerPointsTxtRecord != null ) m_playerPointsTxtRecord.text = PlayerPrefs.GetInt ( "Recorde pontos" ).ToString ( );
    }

    #endregion

    #region GameOver / Restart Game

    //quando o zumbi ataca o player
    public void SetStatusTxtGameOver ( )
    {
        if ( !GameManager._Instance.m_playerController.m_hp.IsAlive ( ) )
        {
            //save record
            GameManager._Instance.SaveRecord ( );
            //update panel
            UpdateGameOverPanel ( );
            //mostra game over
            GameManager._Instance.ShowPanel ( gameObject );
            //esconder panel persistent
            GameManager._Instance.m_persistentController.gameObject.SetActive ( false );
            //animacao player morto
            GameManager._Instance.m_playerController.SetAnimationDie ( true );
        }
    }

    //restart game
    public void RestartGame ( )
    {
        if ( !GameManager._Instance.m_playerController.m_hp.IsAlive ( ) )
        {
            //esconde game over
            GameManager._Instance.HidePanel ( gameObject );
            //mostrar panel persistent
            GameManager._Instance.m_persistentController.gameObject.SetActive ( true );
            //animacao idle
            GameManager._Instance.m_playerController.SetAnimationDie ( false );

            //load na cena
            GameManager._Instance.RestartScene ( );
        }
    }

    #endregion

    
}
