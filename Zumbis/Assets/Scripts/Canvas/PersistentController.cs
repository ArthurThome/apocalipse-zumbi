﻿// Arthur Martins Thomé
// 24 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

#endregion

public class PersistentController : MonoBehaviour
{
    #region Fields

    [SerializeField] private Slider                 m_playerLifeBar         = null; //barra de vida do jogador
    [SerializeField] private TextMeshProUGUI        m_playerPointsTxt       = null; //pontos do jogador
    [SerializeField] private TextMeshProUGUI        m_liveTimee             = null; //tempo vivo
    [SerializeField] public RectTransform           m_panelInteract         = null; //painel do botao E

    public int                                      m_playerPoints          = 0;    //pontuacao do player
    

    #endregion

    private void Awake ( )
    {
        StartCoroutine ( LiveTime ( ) );
    }

    #region Update Life Bar

    public void UpdatePersistentCanvas ( )
    {
        //barra de vida
        if ( m_playerLifeBar != null ) m_playerLifeBar.value = GameManager._Instance.m_playerController.m_hp.GetHPPercentage ( );
        //pontos do player
        if ( m_playerPointsTxt != null ) m_playerPointsTxt.text = m_playerPoints.ToString ( );
    }

    #endregion

    public int GetPoints ( )
    {
        return m_playerPoints;
    }

    public void AddPoint ( int _points )
    {
        m_playerPoints += _points;

        UpdatePersistentCanvas ( );
    }

    private IEnumerator LiveTime ( )
    {
        while ( GameManager._Instance.m_playerController.m_hp.IsAlive ( ) )
        {
            //tempo que esta vivo
            if ( m_liveTimee != null ) m_liveTimee.text = GameManager._Instance.GetTime ( );
            yield return null;
        }

        //resetar tempo
        GameManager._Instance.ResetTime ( );
    }

    #region Button Pause

    public void ButtonPauseGame ( )
    {
        Time.timeScale = 0;
        //mostra painel
        GameManager._Instance.ShowPanel ( GameManager._Instance.m_panelPause.gameObject );
    }

    public void ClosePause ( )
    {
        Time.timeScale = 1;
        //esconde painel
        GameManager._Instance.HidePanel ( GameManager._Instance.m_panelPause.gameObject );
    }

    #endregion
    
}
