﻿// Arthur Martins Thomé
// 23 APR 2019

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class CameraManager : MonoBehaviour
{
    #region Fields

    private new Transform               transform       = null;
    private Vector3                     m_posicao       = new Vector3 ( );

    [SerializeField] private Transform  m_player        = null;                 //instancia do player

    #endregion

    #region MonoBehavior Methods

    private void Start ( )
    {
        transform = GetComponent<Transform> ( );
        //calcula onde a camera vai ficar
        m_posicao = transform.position - m_player.position;
    }

    private void Update ( )
    {
        //trava a camera na posicao
        transform.position = m_player.position + m_posicao;
    }

    #endregion
}
