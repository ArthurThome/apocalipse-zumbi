﻿// Arthur Martins Thomé
// 23 APR 2019

#region Statements

using System.Collections;

using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

#endregion

public class GameManager : MonoBehaviour
{
    #region Singleton

    private static GameManager _instance = null;
    public  static GameManager _Instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<GameManager> ( );
                if ( _instance == null ) Debug.LogError ( "No Game Manager on scene!" );
            }
            return _instance;
        }
    }

    #endregion

    #region Fields
    
    [SerializeField] public Transform               m_playerTransform       = null;     //posicao player

    [ Header ( "Cronometro" ) ]
    private float m_timer = 0;
    private string m_seconds;
    private string m_minutes;
    
    [SerializeField] public AudioSource             m_audioController       = null;     //script audio controller
    [SerializeField] public PlayerController        m_playerController      = null;     //script player
    [SerializeField] public Zombie        m_zombieController      = null;     //script zombie
    [SerializeField] public ZombieGenerator         m_zombieGenerator       = null;     //script zombie generator
    [SerializeField] public ItemController          m_itemController        = null;     //script itemController

    [ Header ( "Panels" ) ]
    [SerializeField] public GameOverController      m_panelGameOver         = null;     //panel game over
    [SerializeField] public PersistentController    m_persistentController  = null;     //panel persistent canvas
    [SerializeField] public PanelPauseController    m_panelPause            = null;     //panel pause

    private byte                                    m_qtdBossAlive          = 0;        //quantos boss no game
    private byte                                    m_qtdBossTotal          = 1;        //maximo de boss do game

    #endregion

    #region Properties

    public byte MQtdBossAlive { get { return m_qtdBossAlive; } set { m_qtdBossAlive = value; } }
    public byte MQtdBossTotal { get { return m_qtdBossTotal; } set { m_qtdBossTotal = value; } }

    #endregion

    #region MonoBehavior Methods

    public void Awake ( )
    {
        ShowPanel ( m_persistentController.gameObject );
        HidePanel ( m_panelPause.gameObject );
        HidePanel ( m_panelGameOver.gameObject );

        m_persistentController.UpdatePersistentCanvas ( );

        StartCoroutine ( TimerDificulty ( ) );
    }

    #endregion

    public void RestartScene ( )
    {
        StopAllCoroutines ( );

        SceneManager.LoadScene ( "Game" );

        m_qtdBossTotal = 1;
    }

    public void ResetTime ( )
    {
        m_timer = 0;
    }

    public string GetTime ( )
    {
        m_timer += Time.deltaTime;

        m_minutes = Mathf.Floor ( m_timer / 60 ).ToString ( "00" );
        m_seconds = Mathf.Floor ( m_timer % 60 ).ToString ( "00" );

        return string.Format ( "{0}:{1}" , m_minutes, m_seconds ).ToString ( );
    }

    public string GetTimeRecord ( )
    {
        //m_timer = PlayerPrefs.GetFloat ( "Recorde Tempo" );

        m_minutes = Mathf.Floor ( PlayerPrefs.GetFloat ( "Recorde Tempo" ) / 60 ).ToString ( "00" );
        m_seconds = Mathf.Floor ( PlayerPrefs.GetFloat ( "Recorde Tempo" ) % 60 ).ToString ( "00" );

        return string.Format ( "{0}:{1}" , m_minutes, m_seconds ).ToString ( );
    }

    public float GetTimeFloat ( )
    {
        return m_timer;
    }

    public void ShowPanel ( GameObject _panel )
    {
        _panel.SetActive ( true );
    }

    public void HidePanel ( GameObject _panel )
    {
        _panel.SetActive ( false );
    }

    #region Records

    public void SaveRecord ( )
    {
        if ( GameManager._Instance.GetTimeFloat ( ) > PlayerPrefs.GetFloat ( "Recorde Tempo" ) )
        {
            Debug.Log("time: " + PlayerPrefs.GetFloat ( "Recorde Tempo" ) );
            //salvar record
            PlayerPrefs.SetFloat ( "Recorde Tempo", GetTimeFloat ( ) );
        }

        if ( GameManager._Instance.m_persistentController.GetPoints ( ) > PlayerPrefs.GetInt ( "Recorde pontos" ) )
        {
            //salvar pontos
            PlayerPrefs.SetInt ( "Recorde pontos", GameManager._Instance.m_persistentController.GetPoints ( ) );
        }
    }

    #endregion

    private IEnumerator TimerDificulty ( )
    {
        do
        {
            yield return new WaitForSeconds ( 60 );

            m_qtdBossTotal++;
        } while ( true );
    }
}
